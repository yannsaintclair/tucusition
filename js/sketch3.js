gammedecouleur = ["#3d61e9","#3ee8aa","#ff485e","#fff352","#ffd6da"];
var bg;
var offsetX, offsetY;
var mousePressedX=0;
var mousePressedY=0;

var obCircle = {
  "name":"obCircle",
  "color":"",
  "dragging":false,
  "drawing": function(){
    circle(obCircle.x, obCircle.y, obCircle.diameter, obCircle.color);
  },
  "ui": {
    "function":function(){
              XYUi(obCircle);
             },
    "input": [{"id":"diameter"}]
  }
};

var obRectangle = {
  "name":"obRectangle",
  "color":"",
  "dragging":false,
  "drawing": function (){
    rectangle(obRectangle.x, obRectangle.y, obRectangle.width, obRectangle.height,obRectangle.color);
  },
  "ui": {
    "function":function(){
              XYUi(obRectangle);
             },
    "input": [{"id":"height"
              },
              {"id":"width"
              }
    ]
  }
};

var obRectangleWhiteDot = {
  "name":"obRectangleWhiteDot",
  "color":"",
  "dragging":false,
  "dotweight":30,
  "interval":60,
  "dotcolor":"#FFFFFF",
  "drawing":function(){
    rectangleDotW(obRectangleWhiteDot.x, obRectangleWhiteDot.y, obRectangleWhiteDot.color, obRectangleWhiteDot.dotweight, obRectangleWhiteDot.interval, obRectangleWhiteDot.dotcolor);
  },
  "ui": {
    "function":function(){
              XYUi(obRectangleWhiteDot);
             },
    "input": [{"id":"height"
              },
              {"id":"width"
              }
    ]
  }
};

var obRectangleBlackDot = {
  "name":"obRectangleBlackDot",
  "color":"#FFFFFF",
  "dragging":false,
  "dotweight":15,
  "interval":30,
  "dotcolor":"#000000",
  "drawing":function(){
    rectangleDotB(obRectangleBlackDot.x, obRectangleBlackDot.y, obRectangleBlackDot.color, obRectangleBlackDot.dotweight, obRectangleBlackDot.interval, obRectangleBlackDot.dotcolor);
  },
  "ui": {
    "function":function(){
              XYUi(obRectangleBlackDot);
             },
    "input": [{"id":"height"
              },
              {"id":"width"
              }
    ]
  }
};

// var obWave = {
//   "name":"obWave",
//   "color":"#FFFFFF",
//   "dragging":false,
//   "drawing": function(){
//     wave(obWave.x,obWave.y, obWave.width, obWave.y1, obWave.y2, obWave.color);
//   },
//   "ui": {
//     "function":function(){
//               XYUi(obWave);
//              },
//     "input": [{"id":"height"
//               },
//               {"id":"width"
//               }
//     ]
//   }
// }


function setup(){
	createCanvas(windowWidth, windowHeight);
   frameRate(5);
  bg = choisirUneCouleur();
  background(bg);
  document.getElementById("wcanvas").value = "";
  document.getElementById("hcanvas").value = "";
  placeholderCanvasSize();
}

function draw(){
  obRectangle.mouseinside = mouseX > obRectangle.x && mouseX < obRectangle.x + obRectangle.width && mouseY > obRectangle.y && mouseY < obRectangle.y + obRectangle.height;
  obRectangleWhiteDot.mouseinside = mouseX > obRectangleWhiteDot.x && mouseX < obRectangleWhiteDot.x + obRectangleWhiteDot.width && mouseY > obRectangleWhiteDot.y && mouseY < obRectangleWhiteDot.y + obRectangleWhiteDot.height;
  obRectangleBlackDot.mouseinside = mouseX > obRectangleBlackDot.x && mouseX < obRectangleBlackDot.x + obRectangleBlackDot.width && mouseY > obRectangleBlackDot.y && mouseY < obRectangleBlackDot.y + obRectangleBlackDot.height;
  //console.log("x : "+obRectangleBlackDot.x+" , width : "+obRectangleBlackDot.width);
  obCircle.mouseinside = dist(obCircle.x, obCircle.y, mouseX, mouseY)<obCircle.diameter/2;
  //obWave.mouseinside = mouseX > obWave.x && mouseX < obWave.x+obWave.width && mouseY < obWave.inf && mouseY > obWave.sup;

  isUi = document.getElementById("ui");
  if (isUi) {
    for (var i = 0; i < listShapes.length; i++) {
      if (listShapes[i].clicked) {
          document.getElementById("x").setAttribute("placeholder", listShapes[i].x);
          //document.getElementById("x").value = listShapes[i].x;
          document.getElementById("y").setAttribute("placeholder", listShapes[i].y);
          //document.getElementById("y").value = listShapes[i].y;
      }
    }
  }

  //draggability
  for (var i = 0; i < listShapes.length; i++) {
    if (listShapes[i].dragging && mousePressedX != mouseX || listShapes[i].dragging && mousePressedY != mouseY) {
      listShapes[i].x = mouseX + offsetX;
      listShapes[i].y = mouseY + offsetY;
    }
  }

  background(bg);
  noStroke();
  obRectangle.drawing();
  obRectangleWhiteDot.drawing();
  obRectangleBlackDot.drawing();
  obCircle.drawing();
  //obWave.drawing();

// draw green border if clicked
  for (var i = 0; i < listShapes.length; i++) {
    if (listShapes[i].clicked) {
      listShapes[i].clicked = true;
      strokeWeight(2);
      stroke("#6ff542");
      listShapes[i].drawing();
    }
  }

  //----------------------------------------OVER EFFECT---------------------------------------------------
  //----------------------------------------OVER EFFECT---------------------------------------------------
  //shortcut for the condition, we don't want to allow the rollover if you are dragging
  var array = [!obRectangle.dragging, !obRectangleWhiteDot.dragging,!obRectangleBlackDot.dragging,!obCircle.dragging,]; //!obWave.dragging
  function checkNoDragging(array) {
    return array == true;
  }

  if (obRectangle.mouseinside && array.every(checkNoDragging) && !obRectangle.clicked ) {
    strokeWeight(4);
    stroke("#00a1d3");
    rectangle(obRectangle.x, obRectangle.y, obRectangle.width, obRectangle.height,obRectangle.color);
    obRectangle.rollover = true;
    obRectangleWhiteDot.rollover = false;
    obRectangleBlackDot.rollover = false;
    obCircle.rollover = false;
    //obWave.rollover = false;
  } else if (obRectangleWhiteDot.mouseinside && array.every(checkNoDragging) && !obRectangleWhiteDot.clicked){
    strokeWeight(4);
    stroke("#00a1d3");
    rectangleDotW(obRectangleWhiteDot.x, obRectangleWhiteDot.y, obRectangleWhiteDot.color, obRectangleWhiteDot.dotweight, obRectangleWhiteDot.interval, obRectangleWhiteDot.dotcolor);
    obRectangleWhiteDot.rollover = true;
    obRectangle.rollover = false;
    obRectangleBlackDot.rollover = false;
    obCircle.rollover = false;
    //obWave.rollover = false;
  } else if (obRectangleBlackDot.mouseinside && array.every(checkNoDragging) && !obRectangleBlackDot.clicked){
    strokeWeight(4);
    stroke("#00a1d3");
    rectangleDotB(obRectangleBlackDot.x, obRectangleBlackDot.y, obRectangleBlackDot.color, obRectangleBlackDot.dotweight, obRectangleBlackDot.interval, obRectangleBlackDot.dotcolor);
    obRectangleBlackDot.rollover = true;
    obRectangle.rollover = false;
    obRectangleWhiteDot.rollover = false;
    obCircle.rollover = false;
    //obWave.rollover = false;
  } else if (obCircle.mouseinside && array.every(checkNoDragging) && !obCircle.clicked) {
    strokeWeight(4);
    stroke("#00a1d3");
    circle(obCircle.x, obCircle.y, obCircle.diameter, obCircle.color);
    obCircle.rollover = true;
    obRectangle.rollover = false;
    obRectangleWhiteDot.rollover = false;
    obRectangleBlackDot.rollover = false;
    //obWave.rollover = false;
  } //else if (obWave.mouseinside && array.every(checkNoDragging) && !obWave.clicked) {
  //   strokeWeight(4);
  //   stroke("#00a1d3");
  //   wave(obWave.x, obWave.y, obWave.width, obWave.y1, obWave.y2, obWave.color);
  //   obWave.rollover = true;
  //   obRectangle.rollover = false;
  //   obRectangleWhiteDot.rollover = false;
  //   obRectangleBlackDot.rollover = false;
  //   obCircle.rollover = false;
  // }
  else {
    obRectangle.rollover = false;
    obRectangleWhiteDot.rollover = false;
    obRectangleBlackDot.rollover = false;
    obCircle.rollover = false;
    //obWave.rollover = false;
    noStroke();
  }
  //------------------------------------END OVER EFFECT---------------------------------------------------
  //------------------------------------END OVER EFFECT---------------------------------------------------

}  // end function draw

var listShapes = [obRectangle,obRectangleWhiteDot, obRectangleBlackDot, obCircle];

function mousePressed() {
  //check if I don't pressed on any shape
  var arrayMouseInside = [!obRectangle.mouseinside, !obRectangleWhiteDot.mouseinside,!obRectangleBlackDot.mouseinside,!obCircle.mouseinside]; //,!obWave.mouseinside
  function checkNoMouseInside(arrayMouseInside) {
    return arrayMouseInside == true;
  }
      //check if we click on the user interface on the top
     uiClickListener = document.getElementById("gui").contains(event.target);
  for (var i = 0; i < listShapes.length; i++) {
    if (listShapes[i].mouseinside && listShapes[i].rollover && !uiClickListener || listShapes[i].mouseinside && listShapes[i].clicked ) {
      mousePressedX = mouseX;
      mousePressedY = mouseY;
      //remove all "clicked" because we only want one clicked at a time
      for (var g = 0; g < listShapes.length; g++) {
        listShapes[g].clicked = false;
      }
      listShapes[i].clicked = true;
      listShapes[i].ui.function();
      listShapes[i].dragging = true;
      offsetX = listShapes[i].x-mouseX;
      offsetY = listShapes[i].y-mouseY;
      for (var a = 0; a < listShapes.length; a++) {
        listShapes[a].rollover = false;
      }
    } else if (arrayMouseInside.every(checkNoMouseInside) && listShapes[i].clicked && !uiClickListener) {
         if(isUi){
           removeUi();
         }
      listShapes[i].clicked = false;
    }
  }
}


function updateXYui(){
  this.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {

      var inputX = document.getElementById("x");
      var inputY = document.getElementById("y");

      inputwidth = document.getElementById("width");
      inputheight = document.getElementById("height");
      inputdiameter = document.getElementById("diameter");

      if (inputwidth) {
        console.log("width exist");
      } else {
        console.log("dont exist");
      }
      // for (var i = 0; i < listShapes.length; i++) {
      //   var forme = listShapes[i];
      //   for (var i = 0; i < forme.ui.input.length; i++) {
      //     var b = forme.ui.input[i].id
      //     eval('var ' + "input" + b + ' = '+ 'document.getElementById("'+b+'");');
      //
      //   }
      // }


      for (var i = 0; i < listShapes.length; i++) {
        if (listShapes[i].clicked && inputX.value && !inputY.value) {
          listShapes[i].x = parseInt(inputX.value);
          inputX.value = "";
        }else if (listShapes[i].clicked && inputY.value && !inputX.value) {
          listShapes[i].y = parseInt(inputY.value);
          inputY.value = "";
        } else if (listShapes[i].clicked && !obCircle.clicked && inputheight.value && !inputdiameter.value) {
          listShapes[i].height = parseInt(inputheight.value);
          inputheight.value = "";
        }else if (listShapes[i].clicked && inputdiameter.value) {
          listShapes[i].diameter = parseInt(inputdiameter.value);
          inputdiameter.value = "";
        } else if (listShapes[i].clicked && inputX.value  && inputY.value) {
          listShapes[i].y = parseInt(inputY.value);
          listShapes[i].x = parseInt(inputX.value);
          inputY.value = "";
          inputX.value = "";
        }
      }
    }
  })
}

function XYUi(shape){
    if(isUi){
      removeUi();
    }

  const div = document.createElement('div');
  div.id = 'ui';

  var puthtml = [];
  for (var i = 0; i < shape.ui.input.length; i++) {
    puthtml[i] = shape.ui.input[i].html;
    // console.log(shape.ui.input[i].html)
  }
  displayInput = '<label><span>X</span><input type="number" name="x" value="" min="0" id="x" placeholder=" " onfocus="updateXYui()"></label><label><span>Y</span><input type="number" name="y" value="" min="0" id="y" placeholder=" " onfocus="updateXYui()"></label>'+ puthtml.join("");
  div.innerHTML = displayInput;

  document.getElementById('ui-container').appendChild(div);
  for (var i = 0; i < shape.ui.input.length; i++) {
    document.getElementById(shape.ui.input[i].id).setAttribute("placeholder", shape.ui.input[i].placeholder);
  }
  document.getElementById("x").setAttribute("placeholder", shape.x);
  document.getElementById("y").setAttribute("placeholder", shape.y);
}

function removeUi() {
  document.getElementById('ui-container').removeChild(document.getElementById('ui'));
}

function mouseReleased() {
  for (var i = 0; i < listShapes.length; i++) {
    listShapes[i].dragging = false;
  }
}


//---------------------------------------------CANVAS---------------------------------------------------
//---------------------------------------------CANVAS---------------------------------------------------
function placeholderCanvasSize(){
  if (!document.getElementById("wcanvas").value) {
    document.getElementById("wcanvas").setAttribute("placeholder", width);
    if (!document.getElementById("hcanvas").value) {
      document.getElementById("hcanvas").setAttribute("placeholder", height);
    }
  } else if (!document.getElementById("hcanvas").value) {
      document.getElementById("hcanvas").setAttribute("placeholder", height);
      if (!document.getElementById("wcanvas").value) {
        document.getElementById("wcanvas").setAttribute("placeholder", width);
      }
  }
}

function changeCanvasSize(){
  this.addEventListener("keyup", function(event) {
    var wValue = document.getElementById("wcanvas").value;
    var hValue = document.getElementById("hcanvas").value;
    if (event.keyCode === 13) {
      if (wValue != width && wValue) {
          resizeCanvas(wValue, height);
          width = wValue;
          background(choisirUneCouleur());
      } else if (hValue != height && hValue) {
          resizeCanvas(width, hValue);
          height = hValue;
          background(choisirUneCouleur());
      }
    } else if (!wValue || !hValue) {
      placeholderCanvasSize();
    }
  });
}
//------------------------------------------END CANVAS---------------------------------------------------
//------------------------------------------END CANVAS---------------------------------------------------

function generateshapes(){

  bg = choisirUneCouleur();
  background(bg);


  for (var i = 0; i < listShapes.length; i++) {
    listShapes[i].x = generateRandom(1,width);
    listShapes[i].y = generateRandom(1,height);
  }

  obRectangle.width = generateRandom(50,width);
  obRectangle.height = generateRandom(50,height);
  obRectangle.color = choisirUneCouleur();

  obRectangleWhiteDot.color = choisirUneCouleur();
  obRectangleWhiteDot.width = generateRandom(0,(width/obRectangleWhiteDot.interval)) * obRectangleWhiteDot.interval + obRectangleWhiteDot.dotweight;
  obRectangleWhiteDot.height = generateRandom(0,(height/(obRectangleWhiteDot.interval)))*obRectangleWhiteDot.interval + obRectangleWhiteDot.dotweight;

 obRectangleBlackDot.width = generateRandom(0,(width/(obRectangleBlackDot.interval))) * obRectangleBlackDot.interval + obRectangleBlackDot.dotweight;
 obRectangleBlackDot.height = generateRandom(0,(height/(obRectangleBlackDot.interval))) * obRectangleBlackDot.interval + obRectangleBlackDot.dotweight;

 obCircle.diameter = generateRandom(1,width);
 obCircle.color = choisirUneCouleur();

 // obWave.width = generateRandom(obWave.x, width-obWave.x);
 // obWave.y1 = generateRandom(-height/2,height/2);
 // obWave.y2 = generateRandom(-obWave.y1/2, obWave.y1/2);
buildingUiInput();

}

function buildingUiInput(){
  for (var i = 0; i < listShapes.length; i++) {
    var forme = listShapes[i];
    for (var a = 0; a < forme.ui.input.length; a++) {
      var b = forme.ui.input[a].id;
      forme.ui.input[a].placeholder = forme[b];
      forme.ui.input[a].html = '<label><span>'+b+'</span><input type="number" name="'+b+'" value="" min="0" id="'+b+'" placeholder=" " onfocus="updateXYui()"></label>';
    }
  }
}

//----------------------CREATE SHAPE--------------------------------------------------------------------------
//----------------------CREATE SHAPE--------------------------------------------------------------------------

function rectangle( x, y, width, height, color){
  obRectangle.x = x;
  obRectangle.y = y;
  obRectangle.width = width;
  obRectangle.height = height;
	fill(color);
	rect(x,y,width,height);
}

function circle( x, y, diameter, color ){
  obCircle.x = x;
  obCircle.y = y;
  obCircle.diameter = diameter;
	fill(color);
	ellipse(x,y,diameter,diameter);
}

function rectangleDotB( x, y, color, dotweight, interval, dotcolor ){
	fill(color);
  obRectangleBlackDot.x = x;
  obRectangleBlackDot.y = y;
	rect(x,y,obRectangleBlackDot.width,obRectangleBlackDot.height);
  noStroke();
	fill(dotcolor);
	for( var x1 = dotweight/2; x1 <= obRectangleBlackDot.width; x1 += interval ){
		for( var y1 = dotweight/2; y1 <= obRectangleBlackDot.height; y1 += interval ){
			ellipse( x+x1, y+y1, dotweight, dotweight );
		}
	}
}

function rectangleDotW( x, y, color, dotweight, interval, dotcolor ){
	fill(color);
  obRectangleWhiteDot.x = x;
  obRectangleWhiteDot.y = y;
	rect(x,y,obRectangleWhiteDot.width,obRectangleWhiteDot.height);
  noStroke();
	fill(dotcolor);
	for( var x1 = dotweight/2; x1 <= obRectangleWhiteDot.width; x1 += interval ){
		for( var y1 = dotweight/2; y1 <= obRectangleWhiteDot.height; y1 += interval ){
			ellipse( x+x1, y+y1, dotweight, dotweight );
		}
	}
}


// function wave(x, y, width, y1, y2, color){
// y1 = y+y1;
// y2 = y1+y2;
//
// var arrayYs = [y,y1,y2];
// obWave.sup = Math.min(...arrayYs);
// obWave.inf = Math.max(...arrayYs);
// obWave.height = obWave.inf - obWave.sup;
// // console.log(Math.min(...arrayYs));
//
//
// var b = (x+width);
// var c = ((b-x)/4)+x;
// var d = c+((b-x)/4);
// var e = d +((b-x)/4);
// fill(color);
// beginShape();
//   vertex(x, y1);
//   bezierVertex(((c-x)/2)+x, y1, ((c-x)/2)+x, y2, c, y2);
//   bezierVertex(((d-c)/2)+c, y2, ((d-c)/2)+c, y1, d, y1);
//   bezierVertex(((e-d)/2)+d, y1, ((e-d)/2)+d, y2, e, y2);
//   bezierVertex(((b-e)/2)+e, y2, ((b-e)/2)+e, y1, b, y1);
//   vertex(b,y1);
//   vertex(b,y);
//   vertex(x,y);
//   vertex(x,y1);
// endShape();
// }

//----------------------END CREATE SHAPE--------------------------------------------------------------------------
//----------------------END CREATE SHAPE--------------------------------------------------------------------------

function generateRandom(min,max){
  var valeur = floor(random(min,max));
  return valeur;
}

function choisirUneCouleur(a){
  a = gammedecouleur[floor(random(0,5))];
  return a;
}
