var listOfShapes = [];
var listOfShapesAndBg = [];
var generated,testRollover,offsetX,offsetY;
colors = ["#3d61e9","#3ee8aa","#ff485e","#fff352","#ffd6da"];
var mousePressedX=0;
var mousePressedY=0;

function setup(){
    numberOfShapes(2,1,0,0,0);
  	createCanvas(windowWidth, windowHeight);
    frameRate(20);
    background(obCanvas.color);
    noStroke();
}

function draw(){
  if (generated) {
    isInside();
    rollover();
    greenStroke();
    Drag();
    updateXYPlaceholder();
  }
}

function numberOfShapes(numC, numR, numRBDot, numRWDot, numW){
  buildCanvasObject();
  buildCircleObject(numC);
  buildRectangleObject(numR);
}

function buildCircleObject(number){
  for (var i = 0; i < number; i++) {
    var n = i+1;
    window['obCircle'+n] = {
      position:[{name:"x",value:""},{name:"y",value:""}],
      dimensions:[{name:"d",value:""}],
      type:"circle",
      name: 'obCircle'+n,
      color:"",
      drawing: function(){circle(window[this.name]);},
      dragging:false,
      rollover:false,
      clicked:false,
      inside:""
    };
    listOfShapes.push(window['obCircle'+n]);
    listOfShapesAndBg.push(window['obCircle'+n]);
  }
}

function buildRectangleObject(number){
  for (var i = 0; i < number; i++) {
    var n = i+1;
    window['obRectangle'+n] = {
      position:[{name:"x",value:""},{name:"y",value:""}],
      dimensions:[{name:"w",value:""},{name:"h",value:""}],
      type:"rectangle",
      name: 'obRectangle'+n,
      drawing: function(){rectangle(window[this.name]);},
      color:"",
      dragging:false,
      rollover:false,
      clicked:false
    };
    listOfShapes.push(window['obRectangle'+n]);
    listOfShapesAndBg.push(window['obRectangle'+n]);
  }
}

function buildCanvasObject(){
    obCanvas = {
      dimensions:[{name:"w",value:windowWidth},{name:"h",value:windowHeight}],
      type:"canvas",
      color:"#D4D3D3",
      rollover:false,
      clicked:false
    };
    listOfShapesAndBg.push(obCanvas);
}

function generate(){
  generated = true;
  position();
  dimensions();
  setColor();
  displayShapes();
}

function position(){
  for (var i = 0; i < listOfShapes.length; i++) {
    var element = listOfShapes[i];
      element.position[0].value = randomNumber(1,width);
      element.position[1].value = randomNumber(1,height);
  }
}

function dimensions(){
  for (var i = 0; i < listOfShapes.length; i++) {
    var element = listOfShapes[i];
    for (var a = 0; a < element.dimensions.length; a++) {
      element.dimensions[a].value = randomNumber(1,height);
    }
  }
}

function setColor(){
  for (var i = 0; i < listOfShapesAndBg.length; i++) {
    var element = listOfShapesAndBg[i];
    element.color = colors[floor(random(0,5))];
  }
}

function displayShapes(){
  for (var i = 0; i < listOfShapesAndBg.length; i++) {
    var element = listOfShapesAndBg[i];
    if (element.type === "canvas") {
      background(element.color);
    } else if (element.type === "circle") {
      element.drawing();
    } else if (element.type === "rectangle") {
      element.drawing();
    }
  }
}

function isInside(){
  for (var i = 0; i < listOfShapes.length; i++) {
    var element = listOfShapes[i];
    if (element.type === "circle") {
      element.mouseinside = dist(element.position[0].value, element.position[1].value, mouseX, mouseY)<element.dimensions[0].value/2;
    } else if (element.type === "rectangle") {
      element.mouseinside = mouseX > element.position[0].value && mouseX < element.position[0].value + element.dimensions[0].value && mouseY > element.position[1].value && mouseY < element.position[1].value + element.dimensions[1].value;
    }
  }
}

//check if one proprety for each shape is false
function isAllNo(property){
  var arrayProperty = [];
  for (var i = 0; i < listOfShapes.length; i++) {
    arrayProperty.push(listOfShapes[i][property]);
  }
  function isNo(arrayProperty) {
    return arrayProperty == false;
  }
   return arrayProperty.every(isNo);
}

// display the background and every Shapes without any stroke
function displayEverything(){
  background(obCanvas.color);
  for (var a = 0; a < listOfShapes.length; a++) {
    noStroke();
    listOfShapes[a].drawing();
  }
}

//if isInside & !clicked && !dragging then blue stroke
function rollover(){
  for (var i = 0; i < listOfShapes.length; i++) {
    var element = listOfShapes[i];
    if (element.mouseinside && isAllNo("dragging") && !element.clicked && isAllNo("rollover")) { // && isAllNoDragging()
      element.rollover = true;
      displayEverything();
      strokeWeight(4);
      stroke("#00a1d3");
      element.drawing();
      noStroke
    }
  }
  for (var i = 0; i < listOfShapes.length; i++) {
    var element = listOfShapes[i];
    if (!element.mouseinside) {
      element.rollover = false;
    }
  }
  if (isAllNo("mouseinside")) {
    displayEverything();
  }
}

//if clicked && isInside then green stroke
function mousePressed(){
//  console.log("mouseX : "+mouseX+" , mouseY : "+mouseY);
  uiClickListener = document.getElementById("gui").contains(event.target);
  for (var i = 0; i < listOfShapes.length; i++) {
    var shape = listOfShapes[i];
    if (shape.mouseinside && shape.rollover && !uiClickListener || shape.clicked && shape.mouseinside && !shape.rollover && !uiClickListener) {   // with rollover I cant click into 2 shape at the same time
      mousePressedX = mouseX;
      mousePressedY = mouseY;
      //remove all "clicked" because we only want one clicked at a time
      for (var a = 0; a < listOfShapes.length; a++) {
        listOfShapes[a].clicked = false
      }
      obCanvas.clicked = false;
      shape.clicked = true;
      setUi(shape.name);
      if (shape.clicked) {
        shape.dragging = true;
        offsetX = shape.position[0].value-mouseX;
        offsetY = shape.position[1].value-mouseY;
        displayEverything();
      }
    }
  }

  //if clicked && !isInside then nostroke && display canvas width and height
  for (var i = 0; i < listOfShapes.length; i++) {
    var shape = listOfShapes[i];
    if (!isAllNo("clicked") && isAllNo("mouseinside") && !uiClickListener) {
      shape.clicked = false;
      displayEverything();
      setUi("obCanvas");
      obCanvas.clicked = true;
    }
  }
}

function mouseReleased() {
  for (var i = 0; i < listOfShapes.length; i++) {
    listOfShapes[i].dragging = false;
  }
}

function greenStroke(){
  for (var x = 0; x < listOfShapes.length; x++) {
    var element = listOfShapes[x];
    if (element.clicked) {
      strokeWeight(4);
      stroke("#6ff542");
      element.drawing();
    }
  }
}

//if clicked && moved then dragging && move shape
function Drag(){
  for (var i = 0; i < listOfShapes.length; i++) {
    if (listOfShapes[i].dragging && mousePressedX != mouseX || listOfShapes[i].dragging && mousePressedY != mouseY) {
      listOfShapes[i].position[0].value = mouseX + offsetX;
      listOfShapes[i].position[1].value = mouseY + offsetY;
      displayEverything();
      greenStroke();
    }
  }
}

function setUi(shape){
  if(isUi){
    removeUi();
  }
  const div = document.createElement('div');
  div.id = 'ui';
  var puthtml = [];
  //check if the element has a position
  if (window[shape].position != undefined) {
    for (var i = 0; i < window[shape].position.length; i++) {
      puthtml[i] = '<label><span>'+window[shape].position[i].name+'</span><input type="number" name="'+window[shape].position[i].name+'" value="" min="0" id="'+window[shape].position[i].name+'" placeholder="'+window[shape].position[i].value+'" onfocus="updateUI('+window[shape].position[i].name+')"></label>'
    }
  }
  // create the ui for the dimensions
  for (var i = 0; i < window[shape].dimensions.length; i++) {
    puthtml.push('<label><span>'+window[shape].dimensions[i].name+'</span><input type="number" name="'+window[shape].dimensions[i].name+'" value="" min="0" id="'+window[shape].dimensions[i].name+'" placeholder="'+window[shape].dimensions[i].value+'" onfocus="updateUI('+window[shape].dimensions[i].name+')"></label>');
  }
  div.innerHTML = puthtml.join("");
  document.getElementById('ui-container').appendChild(div);
}

//so we dont repeat indefinitly the ui
function removeUi() {
  document.getElementById('ui-container').removeChild(document.getElementById('ui'));
}

function updateXYPlaceholder(){
  isUi = document.getElementById("ui");
  if (isUi) {
    for (var i = 0; i < listOfShapes.length; i++) {
      if (listOfShapes[i].dragging) {
          document.getElementById(listOfShapes[i].position[0].name).setAttribute("placeholder", listOfShapes[i].position[0].value);
          document.getElementById(listOfShapes[i].position[1].name).setAttribute("placeholder", listOfShapes[i].position[1].value);
      }
    }
  }
}

function updateUI(element){
    console.log(element.name);
    this.addEventListener("keyup", function(event) {
        if (event.keyCode === 13 && element.value != "") {
          for (var i = 0; i < listOfShapesAndBg.length; i++) {
            var shape = listOfShapesAndBg[i];
            if (shape.clicked  && element.value != "") {
              console.log(shape.name+" clicked");
              if (shape.position != undefined) {
                for (var i = 0; i < shape.position.length; i++) {
                  if (shape.position[i].name == element.name && element.value != "") {
                    console.log("position validé")
                    shape.position[i].value = parseInt(element.value);
                    element.value = "";
                  }
                }
                for (var i = 0; i < shape.dimensions.length; i++) {
                  if (shape.dimensions[i].name == element.name && element.value != "") {
                    console.log("dimension validé")
                    shape.dimensions[i].value = parseInt(element.value);
                    element.placeholder = parseInt(element.value);
                    element.value = "";
                  }
                }
              }

            } else if(isAllNo("clicked") && element.value != ""){
              for (var i = 0; i < obCanvas.dimensions.length; i++) {
                if (obCanvas.dimensions[i].name == element.name && element.value != "") {
                  console.log("valide")
                  obCanvas.dimensions[i].value = parseInt(element.value);
                  element.placeholder = parseInt(element.value);
                  element.value = "";
                  resizeCanvas(obCanvas.dimensions[0].value, obCanvas.dimensions[1].value);
                }
              }
            }
          }
        }
    })
}

//----------------------CREATE SHAPE--------------------------------------------------------------------------
//----------------------CREATE SHAPE--------------------------------------------------------------------------

function circle(obj){
	fill(obj.color);
	ellipse(obj.position[0].value,obj.position[1].value,obj.dimensions[0].value,obj.dimensions[0].value);
}

function rectangle(obj){
  fill(obj.color);
  rect(obj.position[0].value,obj.position[1].value,obj.dimensions[0].value,obj.dimensions[1].value);
}
//----------------------END CREATE SHAPE--------------------------------------------------------------------------
//----------------------END CREATE SHAPE--------------------------------------------------------------------------


function randomNumber(min,max){
  var valeur = floor(random(min,max));
  return valeur;
}
